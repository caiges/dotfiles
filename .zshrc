# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
#ZSH_THEME="robbyrussell"
ZSH_THEME="philips"
#ZSH_THEME="cnichols"

# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# Necessary to make rake work inside of zsh
alias rake='noglob rake'

# Alias for Sublime Text 2
alias sbl='open -a "Sublime Text 2"'

# Alias to 'ls -la'
alias lla='ls -la'

# Alias for common git functions
alias gst='git status'

# Set to this to use case-sensitive completion
# CASE_SENSITIVE="true"

# Comment this out to disable weekly auto-update checks
# DISABLE_AUTO_UPDATE="true"

# Uncomment following line if you want to disable colors in ls
# DISABLE_LS_COLORS="true"

# Uncomment following line if you want to disable autosetting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment following line if you want red dots to be displayed while waiting for completion
# COMPLETION_WAITING_DOTS="true"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=()

source $ZSH/oh-my-zsh.sh

# Customize to your needs...
export PATH=$HOME/.rbenv/bin:/usr/local/share/python:/usr/local/bin:$PATH

# Add Node to path
NODE_PATH=/usr/local/lib/node_modules

# Load rbenv
if which rbenv > /dev/null; then eval "$(rbenv init - zsh)"; fi

# Set some variables
export fullname='Caige Nichols'
export email='cnichols@liquidweb.com'

# Start keychain
eval $(keychain --eval --agents ssh -Q --quiet ~/.ssh/id_rsa)
